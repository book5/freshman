# 業界狀況

## 技術面
目前軟體業以技術來分類的話可以分成下列項目，這裡主要分享 Web Application (system) 在近三年所看到的現況。
* Web Application (system)
* Mobile Application 以及
* Machine Learning (AI)

---
## 公司體制
從公司營運狀況，可以分為下列項目：
* [產品為主](#產品)
* [接案為主](#接案)
* [小型外包工作室](#工作室)
* [MIS / 非資訊業的 IT 部門](#MIS)
* [新創](#新創)

**(一切還是要看每一家公司的真實狀況，以下都是概念上的說明)**

### 產品
產品為主的公司，基本上工程師價值比較高，因為公司的收入就來自於 PG 都產出。有規模的產品公司，
基本上代表有找到一定的 niche，只要公司的發展方向、趨勢都沒有太大問題，基本上比較有機會有更多的資源
(人力、時間、技術)。可能的缺點在於如果產品多數沒有再更新、只進行維護，從新人的角度上來說，
不容易 **「在工作上」** 學習到新的東西。

### 接案
接案公司，代表公司的收入來源都是標案，那很明確的一點，PG 價值多少比產品公司 PG 來的低一些，
畢竟公司業務會努力招到新案子，那除非公司 PG 人數不少、不然很有機會在一年中 (甚至在同一個時期)，
會需要處理多個專案。 但如果公司接的案子性質相似度很高 (例如都是校園資訊系統、警政後台管理系統等等)，
同時又有將功能模組化，相對會輕鬆很多 (但聽到有這樣做的還真的不多)。

### 工作室
有滿多資深工程師或是資深主管，在業界待久了，覺得領死薪水不滿意，會相約幾個相同想法的人出來開工作室做外包案子，
性質跟接案公司很像，不過以新人來說不太容易加入這種團隊，滿常聽到的是在某間公司做了一兩年之後，
主管覺得你的技術能力不錯，就帶著你一起出去走跳。不過就我所知道的是，因為人數不多，所以比起大公司更有機會嘗試
新的技術 (但不一定會成功)。

### MIS
MIS 跟非資訊業的 IT 部門，共同的地方在於，他們都不是公司獲利的主要核心，這意味著 PG 
或是 MIS 人員對於公司的價值都不是很高，即便你認為公司很多運作仰賴電子系統，但依然不是主力，
甚至部門在公司也沒什麼話語權、要不到資源。不過相對壓力不這麼大、比較不需要 on-cal、扛責任。 
簡單來說比較偏向 **「低風險低報酬」** 的狀況。不同的地方是，MIS 人員多數不須 coding 比較多在處理各種業務資訊申請
(例如要申請安裝特別軟體、電腦報修、網路連不上或是要特別開 port...等)。而非資訊業的 IT 部門就會依照部門性質 coding，
工作內容從 Web Application 到 DBA 都有。

### 新創
顧名思義就是新創立的公司，因此就雜，各種技術、各種產品、接案都有。新創公司基本上都是有金主在背後投錢，
所以新創老闆會竭盡所能在資金燒完之前，完成答應的成績單。優點是一定會開高價網羅各種人才，不過缺點就是新創公司成功的少失敗的多，
你不知道哪天會需要重新找工作。 有「聽說」有些新創公司老闆會發現你在當階段沒有辦法馬上達到他的要求
(相對上述其他類型公司、這理的要求通常更高)就會請你離開。


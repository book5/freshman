# Frontend (前端)

* [MVVM](#MVVM)
* [框架](#框架)

# MVVM
再談 MVVM 之前，我們先想想，在撰寫 Mobile Application 的時候，滿多時候我們需要透過串接 API 
存取資料並完成功能。某種程度來說，就像是把前端 (畫面的部分)與後端分離。

接著我們再回頭看看 MVC 框架，controller 最終將資料帶給 view 渲染，或回傳 API 的 JSON 資料，
看似沒什麼問題，架構也確實將畫面渲染邏輯拆分出來。

不過請試想，假設你的 Mobile Application 與 Web Application 功能幾乎相同的強況下，除了原本
導向畫面的 actions (就是寫在 controller 裡面的 functions) 之外，還要再加開幾乎相同邏輯的 
actions 作為 API，或者根據 HTTP request 所夾帶的參數 (request data)，進行判別是要回傳畫面
還是 JSON 資料？

再來，從工程師的角度來看，對於專精於前端的工程師而言，為甚麼要因為專案或產品使用哪一種後端語言或框架，
就要重新熟悉各種不同的渲染引擎語法？例如，假設公司主要都是使用 ASP.NET MVC 作為開發，但因為 ASP.NET
的運行 OS 以及常見對接的 SQL Server，都需要版權，在與他人競爭時相對弱勢，所以要改用 PHP Laravel
進行開發，這時候工程師就要從原本 Razor 改學 Blade。但實際上，對於前端工程師來說，不外乎就是將從
server 所取得的資料綁到 HTML 之中。

因此，在上述的各種問題之下，Web Application 逐漸從原本 MVC 架構，轉為前後端分離，將 view 的部分
另外切出來使用前端框架，資料的存取接改為串接 API 的形式 (意思就是 actions 全都改為回傳 JSON 資料，
同時也意味著後端幾乎成為 API server)。

在拆成前後端分離之後，API 的邏輯都是相同的，所以不論是 Web Application 或是 Mobile Application，
都可以呼叫相同的 API 達到完全相同的功能。一來假設畫面要重寫、換皮，API 不會有異動到；二來，不論何種應用的
界接只要相對應功能都是呼叫相同的 API，則不需要擔心同一個應用會有不同商業邏輯造成功能不一致。

目前檯面上較出名的前端框架都是以 component 為開發宗旨，意思是將畫面上的所有可獨立的元件分別獨立撰寫，
實際的畫面再根據需求，使用各種元件進行拼湊，達到元件重複利用的特性，不用每一個地方都要重新寫一次。

## 框架
因為框架的產生，幫我們優化了網頁渲染的效能。目前主流的前端框架有 Vue、React 與 Angular，這幾個大框架都是使用
ES6 為語言基礎，同時不再使用 JQuery (過於肥大)。

### Vue
* 優點：學習曲線較緩，易學好上手、直觀
* 缺點：台灣社群較小，主戰場位於中國 (因為開發者尤雨溪是中國大大)
* SSR 衍伸框架：Nuxt.js

### React
* 優點：學習曲線中等，台灣最大前端社群，Facebook 在背後撐腰
* 缺點：HTML template 的部分是用 JSX，相對於另外兩個框架還是不直觀，多了太多 JS 語法。
* SSR 衍伸框架：Next.js

### Angular
* 優點：使用 TypeScript、Google 在背後撐腰，同時保哥也是其佈道者
* 缺點：學習曲線陡峭，龐大且複雜
* SSR 衍伸框架：本身就具有 Universal 的專案建立選項



# Backend (後端)

* [MVC](#MVC)
* [語言與框架](#語言與框架)

## MVC
為了確保系統功能的正確性以及降低維護成本，逐漸捨棄了過去傳統「義大利麵式」的 coding 方式，先不論使用各種特別的
design pattern，目前的框架多採用「MVC」的精神，請切記，MVC 只是精神，他強調將各種不同邏輯分離以達到最開始
提到的兩個目標。

> M，代表 Model 意思是將 table schema 轉為 class，且每一筆 table record 都轉為 object。

> V，代表 View 意思是將相關功能的畫面渲染邏輯，統一整合在一個檔案中，基本上就是 HTML 加上一些特殊的渲染語言。

> C，代表 Controller 是整合 DB 存取邏輯、功能商業邏輯，完成功能的所在之處。

事實上，MVC 的設計中，大多數的邏輯依然還是集中在 controller 當中，因此隨著時間一久，controller 還是會愈發肥大
變得無法控制，因此 controller 又可以再多拆成 「Service」 與 「Repository」 兩層。

簡單來說 repository 用來撰寫各種資料庫存取邏輯包括增、刪、改、查以及各種特殊或常用的存取方法；而 service 則是
使用各種 repositories 完成一個「最小且獨立」的功能；最後 controller 則會剩下「資料預處理」、「呼叫各種 services」
、提供 view 所需要的資料或是輸出 JSON 資料成為 API。

由於現在的框架考量的點很多，包括資安、效能等等，因此在實際使用框架時，還會有各種不同功能的資料夾 (我通常稱為各種處理層)，
像是 route、configs、middleware、filler、test...等。

以上的說明都再表達現今多數的後端框架，都會將各項邏輯拆分開來，以便讓系統更有擴充彈性、確保功能的穩定性以及讓系統可維護等
幾個重大目標。所以在學習框架的過程中，不要拘泥於 MVC 到底分別寫在哪個資料夾中，而是要去了解各個框架他主要的精神或是運作
的 data flow。當熟悉框架的結構之後，再去深入瞭解背後的一些技術包括如何提升資料庫效能、使用了那些 design pattern...等。

## 語言與框架
畢竟在巨人的面前，我們都很菜，沒事不要戰語言戰框架。這裡主要列出目前 PHP、Java 和 C# 三個語言常見的相對應 MVC 框架：

### PHP
* 常見框架： `Laravel`、Yii2
* 伺服器：`nginx`、Apache
* 資料庫：MySQL、MongoDB、AWS S3
* 前端渲染引擎：Laravel 主要是用 Blade 或 Vue.js

### Java
* 常見框架： `SpringBoot`
* 伺服器：`Tomcat`
* 資料庫：MySQL、SQL Server
* 前端渲染引擎：thymeleaf

### C#
* 常見框架： `ASP.NET MVC 系列`、`ASP.NET Core`
* 伺服器：`IIS`
* 資料庫：SQL Server
* 前端渲染引擎：Razor
